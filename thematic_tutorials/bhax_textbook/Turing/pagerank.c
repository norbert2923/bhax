#include <stdio.h>
#include <math.h>

int linkCFP(int links[][3], int pagei)
{
    int num = 0;
    for (int i = 0; pagei >= 0 && i < 4; i++)
    {
        if (i == pagei) { continue; }
        for (int j = 0; j <  3; j++)
        {
            if (links[i][j] == pagei)
            {
                num++;
                break;
            }
        }
    }
    return num;
}

double getPRD(double pager[4], double newr[4])
{
    double d = 0.0;
    for (int i = 0; i <= 3; i++)
    {
        double dif = fabs(pager[i] - newr[i]);
        if (dif > d)
        {
            d = dif;
        }
    }
    return d;
}

int main()
{
    int links[4][3] = 
    {
        {1, 2, -1},
        {3, 0, -1},
        {3, 2, -1},
        {1, 2,  0}
    };

    double ranks[4];
    double newr[4];
    double gamma = 0.01;

    //inicializálás
    for (int i = 0; i < 4; i++)
    {
        ranks[i] = 1.0 / 4;
    }

    for (;;)
    {
        //Új állapot
        for (int i = 0; i < 4; i++)
        {
            newr[i] = 0.0;
            for (int j = 0; j < 3; j++)
            {
                int ol = links[i][j];
                if (ol == i)
                {
                    continue;
                }
                if (ol < 0)
                {
                    break;
                }

                int numLink = linkCFP(links, ol);

                newr[i] += ranks[ol] / numLink;
            }
            printf("newrank[%d]=%f \n", i, newr[i]);
        }
        //különbség meghatározása
        double dif = getPRD(ranks, newr);

        //állapot -> aktuális állapot
        for (int i = 0; i <= 3; i++)
        {
            ranks[i] = newr[i];
        }

        //algoritmus vége
        if (dif < gamma)
        {
            break;
        }
    }

    //állapot kiírás
    for (int i = 0; i <= 3; i++)
    {
        printf("Az %d. oldal PageRank értéke: %f\n", i, ranks[i]);
    }
}