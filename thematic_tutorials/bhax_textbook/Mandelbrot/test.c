#include <stdio.h>
#include <unistd.h>
#include <string.h>

int main ()
{
    int keyIndex = 0;
    int keyLength = 4;
    for (int i = 0; i < 10; i++)
    {
        printf("%d\n",keyIndex);
        keyIndex = (keyIndex + 1) % keyLength;
    }
    return 0;
}