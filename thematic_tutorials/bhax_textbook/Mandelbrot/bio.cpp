// Verzio: 3.1.3.cpp
// Forditas:
// g++ 3.1.3.cpp -lpng -O3 -o 3.1.3
// Futtatas:
// ./3.1.3 bmorf.png 800 800 10 -2 2 -2 2 .285 0 10
// Nyomtatas:
// a2ps 3.1.3.cpp -o 3.1.3.cpp.pdf -1 --line-numbers=1  --left-footer="BATF41 HAXOR STR34M" --right-footer="https://bhaxor.blog.hu/" --pro=color
// 
// BHAX Biomorphs
// Copyright (C) 2019
// Norbert Batfai, batfai.norbert@inf.unideb.hu
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
// Version history
//
// https://youtu.be/IJMbgRzY76E
// See also https://www.emis.de/journals/TJNSA/includes/files/articles/Vol9_Iss5_2305--2315_Biomorphs_via_modified_iterations.pdf
//
// Modified by Racs Tamás 2019.03.26.

#include <iostream>
#include "png++/png.hpp"
#include <complex>

int
main ( int argc, char *argv[] )
{

    int width = 1920;
    int height = 1080;
    int iterationLimit = 255;
    double a = -1.9;
    double b = 0.7;
    double c = -1.3;
    double d = 1.3;
    double reC = .285, imC = 0;
    double R = 10.0;

    if ( argc == 12 )
    {
        width = atoi ( argv[2] );
        height =  atoi ( argv[3] );
        iterationLimit =  atoi ( argv[4] );
        a = atof ( argv[5] );
        b = atof ( argv[6] );
        c = atof ( argv[7] );
        d = atof ( argv[8] );
        reC = atof ( argv[9] );
        imC = atof ( argv[10] );
        R = atof ( argv[11] );

    }
    else
    {
        std::cout << "Hasznalat: ./bio fajlnev width height n a b c d reC imC R" << std::endl;
        return -1;
    }

    png::image < png::rgb_pixel > image ( width, height );

    double dx = ( b - a ) / width;
    double dy = ( d - c ) / height;

    std::complex<double> cc ( reC, imC );

    std::cout << "Szamitas\n";

    // j megy a sorokon
    for ( int j = 0; j < height; ++j )
    {
        // k megy az oszlopokon

        for ( int k = 0; k < width; ++k )
        {

            double reZ = a + k * dx;
            double imZ = d - j * dy;
            std::complex<double> z_n ( reZ, imZ );

            int iteration = 0;
            for (int i=0; i < iterationLimit; ++i)
            {

                //z_n = std::pow(z_n, 3) + cc;
                //z_n = std::pow(z_n, 2) + std::sin(z_n) + cc;
                //z_n=std::pow(z_n,z_n)+std::pow(z_n,6);
                z_n=(1.0-std::pow(z_n,3)/6.0)/std::pow((z_n-std::pow(z_n,2.0)/2.0),2)+cc;
                if(std::real ( z_n ) > R || std::imag ( z_n ) > R)
                {
                    iteration = i;
                    break;
                }
            }

            image.set_pixel ( k, j,
                            png::rgb_pixel ( (iteration*20)%255, (iteration*40)%255, (iteration*60)%255 ));
        }

        int percentage = ( double ) j / ( double ) height * 100.0;
        std::cout << "\r" << percentage << "%" << std::flush;
    }

    image.write ( argv[1] );
    std::cout << "\r" << argv[1] << " mentve." << std::endl;

}
