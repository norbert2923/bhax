import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.Timer;

public class Cuboid extends JPanel implements ActionListener, KeyListener
{
    Timer tm = new Timer(5, this);
    int x = 100, y = 100, velocityX = 0, velocityY = 0, velRate = 1;

    public Cuboid()
    {
        tm.start();
        addKeyListener(this);
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
    }

    public void paint(Graphics g)
    {
        super.paintComponent(g);
        g.setColor(Color.GREEN);
        g.fillOval(x, y, 50, 50);
    }

    public void actionPerformed(ActionEvent e)
    {
        x = x + velocityX;
        y = y + velocityY;
        repaint();
    }

    public void keyPressed(KeyEvent e)
    {
        int c = e.getKeyCode();
        char ch = e.getKeyChar();
    
        if (c == KeyEvent.VK_LEFT)
        {
            velocityX = (-1 * velRate);
            velocityY = 0;
        }
        if (c == KeyEvent.VK_RIGHT)
        {
            velocityX = velRate;
            velocityY = 0;
        }
        if (c == KeyEvent.VK_UP)
        {
            velocityX = 0;
            velocityY = (-1 * velRate);
        }
        if (c == KeyEvent.VK_DOWN)
        {
            velocityX = 0;
            velocityY = velRate;
        }
        if (ch == 's')
        {
            velocityX = 0;
            velocityY = 0;
        }
        if ((ch == 'w') && (velRate < 11))
        {
            ++velRate;
        }
        if ((ch == 'e') && (velRate > 1))
        {
            --velRate;
        }
    }

    public void keyTyped(KeyEvent e){}
    public void keyReleased(KeyEvent e){}

    public static void main(String[] argv)
    {
        Cuboid cub = new Cuboid();

        JFrame frame = new JFrame("Fullscreen");

        GraphicsEnvironment graphics = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice device = graphics.getDefaultScreenDevice();

        frame.setUndecorated(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(cub);

        device.setFullScreenWindow(frame);
    }
}

