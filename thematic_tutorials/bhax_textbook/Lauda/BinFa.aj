privileged aspect BinFa 
{
    public pointcut atlag(): call(public double getAtlag());

    after(): atlag()
    {
        System.out.println("-!Az átlag számítása következik!-");
    }
}