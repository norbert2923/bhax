public aspect BAA
{
    public pointcut fgvCall(): call(public void testfgv());

    before(): fgvCall()
    {
        System.out.println("This is the first");
    }

    after(): fgvCall()
    {
        System.out.println("This is the second");
    }
}