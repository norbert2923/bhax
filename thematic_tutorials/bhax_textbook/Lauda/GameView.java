package com.example.flappypaimon;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.media.AudioManager;
import android.media.SoundPool;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.os.Handler;

import androidx.constraintlayout.solver.widgets.Rectangle;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class GameView extends View
{
    Handler handler;
    Runnable runnable;
    final int UPDATE_MILLIS=30;
    Bitmap background;
    Bitmap toptube, bottomtube;
    Display display;
    Point point;
    int dWidth, dHeight;
    Rect rect;
    private Rectangle ttop, tbot, paim;

    private SoundPool soundPool;
    private int valami;
    int i_fs = 1;


    Bitmap[] paimon;

    int  paimonFrame = 0;
    int velocity = 0, gravity = 3;
    int pX, pY;
    boolean gameState = false;

    int gap = 800;
    int minTubeOffset, maxTubeOffset;
    int numberOfTubes = 4;
    int distanceBetweenTubes;
    int[] tubeX = new int[numberOfTubes];
    int[] topTubeY = new int[numberOfTubes];
    Random random;

    int tubeVelocity = 8;


    public GameView(Context context)
    {
        super(context);
        handler = new Handler();
        runnable = new Runnable()
        {
            @Override
            public void run()
            {
                invalidate();
            }
        };
        background = BitmapFactory.decodeResource(getResources(), R.drawable.background_day);
        toptube = BitmapFactory.decodeResource(getResources(), R.drawable.pipe_top);
        bottomtube = BitmapFactory.decodeResource(getResources(), R.drawable.pipe_bottom);
        display = ((Activity) getContext()).getWindowManager().getDefaultDisplay();
        point = new Point();
        display.getSize(point);
        dWidth = point.x;
        dHeight = point.y;
        rect = new Rect(0, 0, dWidth, dHeight);
        paimon = new Bitmap[1];
        paimon[0] = BitmapFactory.decodeResource(getResources(), R.drawable.paimon);
        pX = dWidth / 2 - paimon[0].getWidth() / 2;
        pY = dHeight / 2 - paimon[0].getHeight() / 2;
        distanceBetweenTubes = dWidth * 3 / 4;
        minTubeOffset = gap / 2;
        maxTubeOffset = dHeight - minTubeOffset - gap;
        random = new Random();
        soundPool = new SoundPool(10,AudioManager.STREAM_MUSIC, 0);
        valami = soundPool.load(context, R.raw.score, 1);
        for (int i = 0; i < numberOfTubes; i++)
        {
            tubeX[i] = dWidth + i*distanceBetweenTubes;
            topTubeY[i] = minTubeOffset + random.nextInt(maxTubeOffset - minTubeOffset + 1);
        }
        ttop = new Rectangle();
        tbot = new Rectangle();
        paim = new Rectangle();
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);

        //canvas.drawBitmap(background,0,0,null);
        canvas.drawBitmap(background,null,rect,null);

        if (gameState)
        {
            if (pY < dHeight - paimon[0].getHeight() || velocity < 0)
            {
                velocity += gravity;
                pY += velocity;
            }
            for (int i = 0; i<numberOfTubes; i++)
            {
                tubeX[i] -= tubeVelocity;
                if (tubeX[i] < -toptube.getWidth())
                {
                    tubeX[i] += numberOfTubes * distanceBetweenTubes;
                    topTubeY[i] = minTubeOffset + random.nextInt(maxTubeOffset - minTubeOffset + 1);
                }
                canvas.drawBitmap(toptube, tubeX[i], topTubeY[i] - toptube.getHeight(), null);
                canvas.drawBitmap(bottomtube, tubeX[i], topTubeY[i] + gap, null);

                /*ttop.setBounds(tubeX[i], topTubeY[i] - toptube.getHeight(), toptube.getWidth(), toptube.getHeight());
                tbot.setBounds(tubeX[i], topTubeY[i] + gap, bottomtube.getWidth(), bottomtube.getHeight());

                paim.setBounds(pX, pY, paimon[0].getWidth(), paimon[0].getHeight());

                if ((ttop.x < paim.x + paim.width || tbot.x < paim.x + paim.width) && (ttop.y < paim.y + paim.height || tbot.y < paim.y + paim.height))
                {
                    if (i_fs == 1)
                    {
                        soundPool.play(valami, 1.0f, 1.0f, 0, 0, 1.0f);
                    }
                    if ((ttop.x < paim.x + paim.width || tbot.x < paim.x + paim.width) && (ttop.y < paim.y + paim.height || tbot.y < paim.y + paim.height))
                    {
                        i_fs = 1;
                    }
                }*/
            }
        }

        canvas.drawBitmap(paimon[0], pX, pY,null);
        handler.postDelayed(runnable, UPDATE_MILLIS);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        int action = event.getAction();
        if (action == MotionEvent.ACTION_DOWN)
        {
            velocity = -30;
            gameState = true;
        }

        return true;
    }

}
