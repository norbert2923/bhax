<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Liskov!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>

	<section>
        <title>Liskov helyettesítés sértése</title>
        <para>
            Írjunk olyan OO, leforduló Java és C++ kódcsipetet, amely megsérti a Liskov elvet! Mutassunk rá a megoldásra: jobb OO tervezés.
        </para>
        <para>
            Megoldás forrása: liskov_OO.cpp, liskov_OO.java, Liskov_OO_f.java, liskov_OO_f.cpp
        </para>
        <para>
            A Liskov helyettesítés szerint ha egy S osztály T osztály leszármazottja, akkor T osztály függvényének működni kell a leszármazott, S osztályban is.
        </para>
        <programlisting language="java"><![CDATA[class Madar 
{
    void repul() {};
}

class Program 
{
    void fgv (Madar madar) 
    {

    }
}]]>
        </programlisting>
        <para>
            A szuperosztály a Madár, ez csak egyetlen metódust tartalmaz, aminek nincs visszatérési értéke és az a neve, hogy repül. Ez köznyelvben annyit tedne, hogy feltételezzük, hogy minden madár képes repülni.
        </para>
        <programlisting language="java"><![CDATA[class Sas extends Madar
{

}]]>
        </programlisting>
        <para>
            Ez a sas esetében igaz is, mivel az tényleg képes repülni (ez egy alosztály), na de nézzük tovább.
        </para>
        <programlisting language="java"><![CDATA[class Pingvin extends Madar
{

}]]>
        </programlisting>
        <para>
            A mi világunkban a pingvin sajnos nem tud repülni, így a Liskov által leírt elv hibába ütközik. Ilyen probléma esetén az osztályokat nagyobb odafigyeléssel kell megterveznünk.
        </para>
        <programlisting language="java"><![CDATA[class Madar_f 
{

}]]>
        </programlisting>
        <para>
            A repül metódust kivesszük a Madar_f osztályból, így már nem feltételezzük, hogy minden madár képes repülni.
        </para>
        <programlisting language="java"><![CDATA[class RepuloMadar_f extends Madar_f
{
    void repul() {};
}

class Program_f
{
    void fgv (Madar_f madar_f) 
    {

    }
}]]>
        </programlisting>
        <para>
            Viszont hozzáadunk egy RepuloMadar_f osztályt a programhoz, ami tartalmazza a repul metódust, így megkülönböztetve a sima madarakat a repülőmadaraktól.
        </para>
        <programlisting language="java"><![CDATA[class Sas_f extends RepuloMadar_f
{

}]]>
        </programlisting>
        <para>
            A Sas_f osztályát most már nem a Madar_f osztályból, hanem a RepuloMadar_f osztályból származtatjuk, mivel a sas képes repülni.
        </para>
        <programlisting language="java"><![CDATA[class Pingvin_f extends Madar_f
{

}]]>
        </programlisting>
        <para>
            Ellenben a Pingvin_f osztályt a Madar_f osztályból származtatjuk, mivel a mi világunkban a pingvinek még mindig nem képesek repülni.
        </para>
        <programlisting language="java"><![CDATA[class liskov_OO_f
{
    public static void main(String[] args)
    {
        Program_f program_f = new Program_f();
        Madar_f madar_f = new Madar_f();
        program.fgv (madar_f);
        
        Sas_f sas_f = new Sas_f();
        program.fgv (sas_f);

        Pingvin_f pingvin_f = new Pingvin_f();
        program.fgv (pingvin_f);
    }
}]]>
        </programlisting>
        <para>
            A program ezen része nem változott.
        </para>
        <para>
            Ez a feladat C++-os megvalósításban:
        </para>
        <programlisting language="c++"><![CDATA[class Madar
{
    virtual void repul() {};
};

class Program
{
    public:
        void fgv ( Madar &madar)
        {

        }
};

class Sas : public Madar {};

class Pingvin : public Madar {};

int main(int argc, char **argv)
{
    Program program;
    Madar madar;
    program.fgv (madar);

    Sas sas;
    program.fgv (sas);

    Pingvin pingvin;
    program.fgv (pingvin);
}]]>
        </programlisting>
        <para>
            Elvi szabályokkal:
        </para>
        <programlisting language="c++"><![CDATA[class Madar_f
{
    virtual void repul() {};
};

class Program_f
{
    public:
        void fgv ( Madar_f &madar_f)
        {

        }
};

class Sas_f : public Madar_f {};

class Pingvin_f : public Madar_f {};

int main(int argc, char **argv)
{
    Program_f program;
    Madar_f madar_f;
    program.fgv (madar_f);

    Sas_f sas_f;
    program.fgv (sas_f);

    Pingvin_f pingvin_f;
    program.fgv (pingvin_f);
}]]>
        </programlisting>
    </section>   

	<section>
        <title>Szülő-gyerek</title>
        <para>
            Írjunk Szülő-gyerek Java és C++ osztálydefiníciót, amelyben demonstrálni tudjuk, hogy az ősön keresztül csak az ős üzenetei küldhetőek!
        </para>
        <para>
            Megoldás forrása: szulo_gyerek.java, szulo_gyerek.cpp
        </para>
        <para>
            Minden esetben, mikor egy osztályból létrehozunk egy alosztályt, akkor az alosztály örököl "tulajdonságot" a szuperosztálytól. De ez nem reverzibilis folyamat, vagyis az alosztálytól a szuperosztály nem kaphat tulajdonságot.
        </para>
        <programlisting language="java"><![CDATA[class Szulo
{
    public void viselkedesszulo()
    {
        System.out.println("Szülő viselkedés");
    }
}

class Gyerek extends Szulo
{
    public void viselkedesgyerek()
    {
        System.out.println("Gyerek viselkedés");
    }
}]]>
        </programlisting>
        <para>
            A Gyerek osztályt a Szulo osztályból származtatjuk, de ettől függetlenül mindkét osztálynak megvannak a maga viselkedései.
        </para>
        <programlisting language="java"><![CDATA[class szulo_gyerek
{
    public static void  main(String[] args)
    {
        Szulo szulo = new Gyerek();
        szulo.viselkedesszulo();
    }
}]]>
        </programlisting>
        <para>
            A fő programrészben látható, hogy a Szulo típusú osztály csak a Szulo metódusát képes felismerni, de a Gyerek metódusát nem.
        </para>
        <para>
            C++-os megvalósítás:
        </para>
        <programlisting language="c++"><![CDATA[#include <iostream>

using namespace std;

class Szulo
{
    public:
        void szuloviselkedes()
        {
            cout << "Szülő viselkedés" << endl;
        }
};

class Gyerek : public Szulo
{
    virtual void gyerekviselkedes()
    {
        cout << "Gyerek viselkedés" << endl;
    }
};

int main (int argc, char *argv[])
{
    Szulo szulo;

    szulo.szuloviselkedes();
}]]>
        </programlisting>
    </section> 

    <section>
        <title>Anti OO</title>
        <para>
            A BBP algoritmussal 4 a Pi hexadecimális kifejtésének a 0. pozíciótól számított 106, 107, 108 darab jegyét határozzuk meg C, C++, Java, és C# nyelveken és vessük össze a futási időket! <link xlink:href="https://www.tankonyvtar.hu/hu/tartalom/tkt/javat-tanitok-javat/apas03.html#id561066">https://www.tankonyvtar.hu/hu/tartalom/tkt/javat-tanitok-javat/apas03.html#id561066</link>
        </para>
        <para>
            Megoldás forrása: BBP.c, BPP.java, BPP.cpp, BPP.cs
        </para>
        <programlisting language="java"><![CDATA[class PiBPP
{
    
    char[] hexLetters = {'A','B','C','D','E', 'F'};
    PiBPP(int n)
    {
        double pi = 0.0;
        double pi1 = 4* getS(n,1);
        double pi2 = 2*getS(n,4);
        double pi3 = getS(n,5);
        double pi4 = getS(n,6);
        
        pi = pi1-pi2-pi3-pi4;
        pi = pi-StrictMath.floor(pi);
        
        StringBuilder str = new StringBuilder();
        
        while(pi != 0.0)
        {
            int hex = (int)StrictMath.floor(16.0*pi);
            str.append((hex < 10) ? String.valueOf(hex) : hexLetters[hex-10]);
            
          
            pi = 16.0*pi - StrictMath.floor(16.0*pi);
            
        }
        
        System.out.println(str.toString());
        
    }
    
    private long nMod(int n, int k)
    {
        int t = 1;
        
        while(t <= n)
        {
            t = 2*t;
            
        }
        long r = 1;
        
        while(true)
        {
            if(n >= t)
            {
                r = 16*r % k;
                n = n - t;
            }
            t/=2;
            if(t < 1)
                break;
            if(t>=1)
            {
                r = r*r % k;
            }
        }
        
        return r;
        
    }
    
    public double getS(int d, int j)
    {
        double ret = 0.0;
        
        for(int i=0; i<d; i++)
        {
            ret+= (nMod(d-i, 8*i+j))/(double)(8*i+j);
            
        }
        for(int i=d+1; i<2*d; i++)
        {
            ret+= StrictMath.pow(16, d-i)/(double)(8*i+j);
        }
        
        return ret - StrictMath.floor(ret);
    }
    
    public static void main(String[] args)
    {
        new PiBPP(1000000);
        
    }
}
]]>
        </programlisting>
        <para>
            Java implementáció
        </para>
        <programlisting language="c++"><![CDATA[#include <iostream>
#include <string>
#include <cmath>

char hexLetters[] = {'A', 'B', 'C', 'D', 'E', 'F'};

long nMod(int n, int k)
{
    int t = 1;

    while (t <= n)
    {
        t = 2*t;
    }
    long r = 1;
        
        while(true)
        {
            if(n >= t)
            {
                r = 16*r % k;
                n = n - t;
            }
            t/=2;
            if(t < 1)
                break;
            if(t>=1)
            {
                r = r*r % k;
            }
        }
        
        return r;

}

double getS (int d, int j)
{
    double ret = 0.0;
        
    for(int i=0; i<d; i++)
    {
        ret+= (nMod(d-i, 8*i+j))/(double)(8*i+j);       
    }

    return ret - std::floor(ret);
}

void BPP (int n)
{
    double pi = 0.0;
    double pi1 = 4* getS(n,1);
    double pi2 = 2*getS(n,4);
    double pi3 = getS(n,5);
    double pi4 = getS(n,6);
    
    pi = pi1-pi2-pi3-pi4;
    pi = pi-std::floor(pi);

    int hex = std::floor(16.0*pi);
    std::cout << ((hex < 10) ? (char)(hex + '0') : hexLetters[hex-10]);
}

int main(int argc, char* argv[])
{
    int exp = std::atoi(argv[1]);

    long long limit = std::pow(10,exp);

    BPP(limit);
    
    std::cout << std::endl;
    return 0;
}]]>
        </programlisting>
        <para>
            C++ implementáció
        </para>
        <programlisting language="c"><![CDATA[#include <stdio.h>
#include <stdlib.h>
#include <math.h>

char hexLetters[] = {'A','B','C','D','E', 'F'};

long nMod(int n, int k)
{
        int t = 1;
        
        while(t <= n)
        {
            t = 2*t;
            
        }
        long r = 1;
        
        while(1)
        {
            if(n >= t)
            {
                r = 16*r % k;
                n = n - t;
            }
            t/=2;
            if(t < 1)
                break;
            if(t>=1)
            {
                r = r*r % k;
            }
        }
        
        return r;
        
}

double getS(int d, int j)
{
        double ret = 0.0;
        
        for(int i=0; i<d; i++)
        {
            ret+= (nMod(d-i, 8*i+j))/(double)(8*i+j);
            
        }
        
        return ret - floor(ret);
}
void calculate(int n)
{   
    double pi = 0.0;
    double pi1 = 4* getS(n,1);
    double pi2 = 2*getS(n,4);
    double pi3 = getS(n,5);
    double pi4 = getS(n,6);
    
    pi = pi1-pi2-pi3-pi4;
    pi = pi-floor(pi);
    int hex = floor(16.0*pi);
    printf("%c\n",((hex < 10) ? (char)((hex) + '0') : hexLetters[hex-10]));   
}
int main(int argc, char* argv[])
{
        if(argc == 1)
        {
            printf("Usage: ./BPP <exponent>\n");
            exit(-1);
        }
        int exponent = atoi(argv[1]);
        if(!exponent)
        {
            printf("Usage: ./BPP <exponent>\n");
            exit(-1);
        }
        printf("10^%d\n", exponent);
        int limit = pow(10,exponent);
        calculate(limit);               
        
}
]]>
        </programlisting>
        <para>
            C implementáció
        </para>
        <!--<programlisting language="csharp"><![CDATA[using System;

namespace CSHARP
{
    class Program
    {
        public static double getS(int d, int j) 
        {
            
            double ret = 0.0d;
            
            for(int k=0; k<=d; ++k)
                ret += (double)nMod(d-k, 8*k + j) / (double)(8*k + j);
            
            
            return ret - Math.Floor(ret);
        }
    
        public static long nMod(int n, int k) 
        {
            
            int t = 1;
            while(t <= n)
                t *= 2;
            
            long r = 1;
            
            while(true) {
                
                if(n >= t) {
                    r = (16*r) % k;
                    n = n - t;
                }
                
                t = t/2;
                
                if(t < 1)
                    break;
                
                r = (r*r) % k;
                
            }
            
            return r;
        }
        public static void calculate(int n)
        {
            char[] hexLetters = {'A','B','C','D','E','F'};
            double pi = 0.0;
            double pi1 = 4* getS(n,1);
            double pi2 = 2*getS(n,4);
            double pi3 = getS(n,5);
            double pi4 = getS(n,6);
            
            pi = pi1-pi2-pi3-pi4;
            pi = pi-Math.Floor(pi);
            int hex = (int)Math.Floor(16.0*pi);
            Console.WriteLine("A {0}. pozíción lévő jegye {1}",n, ((hex < 10) ? (char)((hex) + '0') : hexLetters[hex-10]));
        }
    
        public static void Main(String[] args)
        {
            Console.WriteLine("10^" + Convert.ToInt32(args[0]));
            calculate((int)Math.Pow(10,Convert.ToInt32(args[0])));
           
        }
            
        }
    }

]]>
        </programlisting>
        <para>
            C# implementáció
        </para>-->
        <para>
            Most nézzük meg a különböző implementációk futási idejeit, ezáltal megtudva, hogy melyik nyelv képes gyorsabban számolni. Három esetet fogunk vizsgálni futtatás során, amikor a d érétke 10^6, 10^7 valamint 10^8
        </para>
        <figure>
            <title>Java implementáció futási ideje</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="Liskov/img/java_time.png" scale="50" />
                </imageobject>
                <textobject>
                    <phrase>Java implementáció futási ideje</phrase>
                </textobject>
            </mediaobject>
        </figure>
        <figure>
            <title>C implementáció futási ideje</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="Liskov/img/c_time.png" scale="50" />
                </imageobject>
                <textobject>
                    <phrase>C implementáció futási ideje</phrase>
                </textobject>
            </mediaobject>
        </figure>
        <figure>
            <title>C++ implementáció futási ideje</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="Liskov/img/cpp_time.png" scale="50" />
                </imageobject>
                <textobject>
                    <phrase>C++ implementáció futási ideje</phrase>
                </textobject>
            </mediaobject>
        </figure>
        <para>
            Mint láthatjuk a Java toronymagasan kiemelkedik a többi nyelvhez képest az osztály struktúrájával.
        </para>
    </section>

    <!--<section>
        <title>deprecated - Hello Android!</title>
        <para>
        	
        </para>
    </section>-->

    <!--<section>
        <title>Hello, Android!</title>
        <para>
        
        </para>
    </section>

    <section>
        <title>Hello, SMNIST for Humans!</title>
        <para>
        
        </para>
    </section>-->

    <section>
        <title>Ciklomatikus komplexitás</title>
        <para>
            Számoljuk ki valamelyik programunk függvényeinek ciklomatikus komplexitását! Lásd a fogalom tekintetében a https://arato.inf.unideb.hu/batfai.norbert/UDPROG/deprecated/Prog2_2.pdf (77-79 fóliát)!
        </para>
        <para>
            Megoldás forrása:
        </para>
        <para>
            A ciklomatikus komplexitás egy metrika, ami ebben a helyzetben a metódusok minőségét hivatott jellemezni. (Emellett adaptálható modulokra és osztályokra is)
        </para>
        <para>
            Ennek az eljárásnak a képlete:
        </para>
        <para>
            M = E - N + 2P
        </para>
        <para>
            Melyben:
        </para>
        <para>
            - E jelenti a gráf éleinek a számát
        </para>
        <para>
            - N a gráfban található csúcsoknak a számát
        </para>
        <para>
            - P pedig az összefüggő komponenseknek a számát
        </para>
        <para>
            Erre a legegyszerűbb mód, hogy ellátogatunk a <link xlink:href="http://www.lizard.ws/">Lizard</link> oldalára, ami egy ciklomatikus komlexitás számítására használható weboldal.
        </para>
        <para>
            Egy példa a egy korábban megírt programmal (OO hexadecimális Pingvin)
        </para>
        <figure>
            <title>Ciklomatikus Komplexitása a Hexadecimális Pi programnak</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="Liskov/img/complexity_pibpp.png" scale="80" />
                </imageobject>
                <textobject>
                    <phrase>Ciklomatikus Komplexitása a Hexadecimális Pi programnak</phrase>
                </textobject>
            </mediaobject>
        </figure>

    </section>

    <!--<section>
        <title>EPAM: Interfész evolúció Java-ban</title>
        <para>
        
        </para>
    </section>

    <section>
        <title>EPAM: Liskov féle helyettesíthetőség elve, öröklődés</title>
        <para>
        
        </para>
    </section>

    <section>
        <title>EPAM: Interfész, Osztály, Absztrakt Osztály</title>
        <para>
        
        </para>
    </section>-->
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
</chapter>                
