#!/bin/bash

exponents=(6 7 8)

echo "A BBP algoritmus mérése a következő gépen:"
neofetch

echo "JAVA program mérési eredményei"

for exponent in ${exponents[*]}
do
	time java PiBPP $((exponent))
	echo
done

echo "C program mérési eredményei"

for exponent in ${exponents[*]}
do
	time ./PiBPPc $((exponent))
	echo
done

echo "C++ program mérési eredményei"

for exponent in ${exponents[*]}
do
	time ./PiBPPcpp $((exponent))
	echo
done

echo "Teszt sikeresen végrehajtva!"
