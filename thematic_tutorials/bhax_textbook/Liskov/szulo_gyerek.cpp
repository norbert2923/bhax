#include <iostream>

using namespace std;

class Szulo
{
    public:
        void szuloviselkedes()
        {
            cout << "Szülő viselkedés" << endl;
        }
};

class Gyerek : public Szulo
{
    virtual void gyerekviselkedes()
    {
        cout << "Gyerek viselkedés" << endl;
    }
};

int main (int argc, char *argv[])
{
    Szulo szulo;

    szulo.szuloviselkedes();
}