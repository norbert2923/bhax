class Madar_f
{
    virtual void repul() {};
};

class Program_f
{
    public:
        void fgv ( Madar_f &madar_f)
        {

        }
};

class Sas_f : public Madar_f {};

class Pingvin_f : public Madar_f {};

int main(int argc, char **argv)
{
    Program_f program;
    Madar_f madar_f;
    program.fgv (madar_f);

    Sas_f sas_f;
    program.fgv (sas_f);

    Pingvin_f pingvin_f;
    program.fgv (pingvin_f);
}