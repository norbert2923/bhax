class Madar 
{
    void repul() {};
}

class Program 
{
    void fgv (Madar madar) 
    {

    }
}

class Sas extends Madar
{

}

class Pingvin extends Madar
{

}

class liskov_OO
{
    public static void main(String[] args)
    {
        Program program = new Program();
        Madar madar = new Madar();
        program.fgv (madar);
        
        Sas sas = new Sas();
        program.fgv (sas);

        Pingvin pingvin = new Pingvin();
        program.fgv (pingvin);
    }
}