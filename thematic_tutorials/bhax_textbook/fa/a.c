#include <stdio.h>

int main()
{
    int *p;
    int (*ptr)[5];

    int array[5]={1,2,3,4,5};

    p = array;
    ptr = &array;

    printf("%p %p \n", p, ptr);
    ++p, ++ptr;
    printf("%p %p \n", p, ptr);

    return 0;
}