#include <stdio.h>
#include <stdlib.h>

int main()
{
    double **tm;

    tm = (double **) malloc(5*sizeof(double *));
    for (int i = 0; i<5; ++i)
        tm[i] = malloc((i+1)*sizeof(double));

    for (int i = 0; i<5; ++i)
        for (int j = 0; j<i+1; ++j)
            tm[i][j] = i;

    *(*(tm+1)+1) = 5.0;

    for (int i = 0; i<5; ++i)
    {
        printf("\n");
        for (int j = 0; j<i+1; ++j)
            printf("%f ", tm[i][j]);
    }

    for (int i = 0; i<5; ++i)
        free(tm[i]);

    free(tm);


    return 0;
}