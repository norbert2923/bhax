#include "z3a18qa5_from_scratch.h"
#include <fstream>

class COVIR19Var {
    private:
        std::string loc;
        ZLWTree<char, '/', '0'> zlwBt;

    public:
        COVIR19Var(std::string loc, std::string filename):loc(loc)
        {
            std::ifstream ifs{filename};
            std::istreambuf_iterator<char> isbiEnd;

            for(std::istreambuf_iterator<char> isbiEnd, isbi{ifs};isbi != isbiEnd; ++isbi)
                zlwBt << *isbi;
        }
        
        void print() {zlwBt.print();}
};

int main()
{
    COVIR19Var cv{"Severe acute respiratory syndrome coronavirus 2 isolate Wuhan-Hu-1, complete genome",genom.txt};
    cv.print();


}