#include <iostream>
#include <vector>
#include <iterator>

int main()
{
    std::vector<int> v;
    v.reserve(5);

    v.push_back(42);
    v.push_back(41);
    v.push_back(40);

    for(std::vector<int>::iterator it = v.begin(); it != v.end(); ++it)
        std::cout << *it;

    std::cout << std::endl;

    return 0; 

}