#include <iostream>

int main()
{
    int nt[] = {1, 2, 3};
    int *end = nt + 3;

    std::cout << nt << std::endl << end << std::endl;

    for(int *it = nt; it != end; ++it)
        std::cout << *it;

    std::cout << std::endl;

    return 0; 

}