#include <iostream>
#include <limits>

void convertDecimalUnary(int val);

int main()
{
    int val, theOption;

    std::cout << "Add meg a számot, amit unárisba szeretnél váltani:\n";

    do
    {
        std::cin >> val;
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    } while (std::cin.fail());

    convertDecimalUnary(val);

    return 0;
}

void convertDecimalUnary(int val)
{
    for (int i = 0; i < val; i++)
    {
        std::cout << "1";
    }
    std::cout << "\n";
}