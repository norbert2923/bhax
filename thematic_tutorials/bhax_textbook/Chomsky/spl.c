#include <stdio.h>

int f (int n, char* d, char* s)
{
    for (int i = 0; i < n && (*d++ = *s++); ++i) ;
}

int main()
{
    char s[] = "1234567890";
    char d[] = "          ";
    f(5, d, s);
    printf("s:[%s]\n", s);
    printf("d:[%s]\n", d);

    char* s2 = "1234567890";
    char d2[] = "                              ";
    f(15, d2, s2);
    printf("s2:[%s]\n", s2);
    printf("d2:[%s]\n", d2);
}