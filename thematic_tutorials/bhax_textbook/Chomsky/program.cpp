#include <iostream>

int *h()
{
    int a = 42;
    int *b = &a;
    std::cout << b << "\n";
    return b;
}

int main()
{
    int a = 42;
    std::cout << "Egész: " << a << "\n";


    int *b = &a;
    std::cout << "Egészre mutató mutató:" << b << "\n";


    int &r = a;
    std::cout << "Egész referenciája: " << a << "\n";


    int t[5] = {1, 2, 3, 4, 5};
    for (int i = 0; i < 5; i++)
    {
        std::cout << "Egészek tömbje: " << t[i] << "\n";
    }


    int (&tr)[5] = t;
    std::cout << "Egészek tömbjének referenciája: " << tr << "\n";


    int *d[5];
    for (int i = 0; i < 5; i++)
    {
        d[i] = &t[i];
        std::cout << "Egészre mutató mutatók tömbje: " << d[i] << "\n";
    }


    std::cout << "Egészre mutató mutatót visszaadó függvény: " << h() << "\n";


    int *(*ptr)() = h;
    std::cout << "Egészet mutató mutatót visszaadó függvényre mutató mutató: " << reinterpret_cast<void*>(ptr) << "\n";

    return 0;
}