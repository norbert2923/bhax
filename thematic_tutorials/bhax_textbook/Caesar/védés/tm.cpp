#include <stdlib.h>
#include <stdio.h>

int main()
{
    double **tm;
    int nr = 5;

    tm = new double*[5];

    for (int i = 0; i < nr; ++i)
    {
        tm[i] = new double[5];
    }

    for (int i = 0; i < nr; ++i)
        for (int j = 0; j < i + 1; ++j)
            tm[i][j] = i + j;

    for (int i = 0; i < nr; ++i)
    {
        for (int j = 0; j < i + 1; ++j)
            printf("%f\t", tm[i][j]);
        printf("\n");
    }

    tm[3][0] = 42.;
    (*(tm + 3))[1] = 43.;
    (*(*(tm + 3)+3)) = 45.;
    (*(tm[3]+2)) = 44.;

    for (int i = 0; i < nr; ++i)
    {
        for (int j = 0; j < i + 1; ++j)
            printf("%f\t", tm[i][j]);
        printf("\n");
    }

    for (int i = 0; i < nr; ++i)
    {
        delete[] tm[i];
    }

    delete[] tm;

    return 0;
}