#include <stdio.h>
#include <unistd.h>
#include <string.h>

void 
XOR_code (char *input, char *output)
{
    char key[] = {'k', 'o', 'd'};

    int i;
    for(i = 0; i < strlen(input); i++)
    {
        output[i] = input[i] ^ key[i % (sizeof(key)/sizeof(char))];
    }
}

int main(int argc, char *argv[])
{
    char baseStr[] = "*";

    char encrypted[strlen(baseStr)];
    XOR_code(baseStr, encrypted);
    printf("Encrypted: %s\n", encrypted);

    char decrypted[strlen(baseStr)];
    XOR_code(encrypted, decrypted);
    printf("Decrypted: %s\n", decrypted);
}